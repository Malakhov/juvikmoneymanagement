// Every Angular app has at least one root component, conventionally named AppComponent, that hosts the client user experience.
// Components are the basic building blocks of Angular applications. A component controls a portion of the screen — a view — through its associated template.

import {Component,OnInit,OnChanges,NgZone,Input,ViewChild} from 'angular2/core';
<<<<<<< HEAD
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS,RouteDefinition,Router } from 'angular2/router';
=======
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS,RouteDefinition } from 'angular2/router';
>>>>>>> origin/master


import { LandingViewComponent } from './landing-view/landing-view.component';
import { TodayViewComponent } from './today-view/today-view.component';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { NavigationComponent } from './navigation/navigation.component';

import { AppService } from './util/app.service';
<<<<<<< HEAD
import { User } from './user/user';
=======
>>>>>>> origin/master


@Component({
    //The element for this component is named my-app. Angular creates and displays an instance of our AppComponent
    // wherever it encounters a my-app element in the host HTML
    selector: 'my-app',
    //A more advanced template could contain data bindings to component properties and might identify other application components
    // which have their own templates. These templates might identify yet other components. In this way an Angular application becomes a tree of components.
    // top-navigation style="display:none;" to hide immidiatly on landing page
    template: `
      <top-navigation  style="display:none;"></top-navigation>
      <router-outlet></router-outlet>
    `,
    //router will control all directives so we need to add only the router derivative here
    directives: [ROUTER_DIRECTIVES,
        NavigationComponent],
    //Services that we will use. We need to add it only one time in the app.component.ts
    providers: [
<<<<<<< HEAD
        ROUTER_PROVIDERS,AppService,User
=======
        ROUTER_PROVIDERS,AppService
>>>>>>> origin/master
    ]
})


// The @RouteConfig decorator simultaneously (a) assign a router to the component and (b) configure that router with routes.
// Routes tell the router which views to display when a user clicks a link or pastes a URL into the browser address bar.
@RouteConfig([
  {
    path: '/',
    name: 'Landing',
    component: LandingViewComponent,
    useAsDefault: true
  },
  {
    path: '/today',
    name: 'Today',
    component: TodayViewComponent
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: DashboardViewComponent
  }
])

//We can expand this class with properties and application logic
//We export AppComponent so that we can import it elsewhere in our application
export class AppComponent implements OnInit{
<<<<<<< HEAD
  constructor(public appServise:AppService,private zone:NgZone,public router:Router, public user:User) {
       // Subscribe for routes changes
        router.subscribe((urlString:string) => {
          // Watch for url parameters statrs from '?userName' - that will appear after successful authorisation
          // with passportjs
            if(urlString.startsWith('?userName')){
              // Get loggedin user
              this.user.getUser(
                //callback function, will be called if user session was started
                (data)=>{
                  // Set this session user
                  this.user.setUser(data.name,data.role,true);
                  // Navigate to the Today view
                  this.router.navigate(['Today']);
                },
                (err)=>{
                   alert("User session is broken. Try to login again.")
                }
              );
            } 
        });
  } 
  @ViewChild(TodayViewComponent) child:TodayViewComponent;
  ngOnInit() {

=======
  constructor(public appServise:AppService,private zone:NgZone) {
  } 
  @ViewChild(TodayViewComponent) child:TodayViewComponent;
  ngOnInit() {
       FB.init({ 
        appId: '1621898574716044', 

        /* 
        Adding a Channel File improves the performance 
        of the javascript SDK, by addressing issues 
        with cross-domain communication in certain browsers. 
        */
        // channelUrl: 'app/channel.html', 

        /* 
        Set if you want to check the authentication status
        at the start up of the app 
        */
        status: true, 

        /* 
        Enable cookies to allow the server to access 
        the session 
        */
        cookie: true, 

        /* Parse XFBML */
        xfbml: true,
        version: 'v2.4' 
      });
      
        this.appServise.caller(FB.getLoginStatus,(response) => {
            if (response.status === 'connected') {
              //If user already logged in 
              this.appServise.isLoggedIn = true;
            }else{
              this.appServise.isLoggedIn = false;
            }
        });
>>>>>>> origin/master
  }
}


  